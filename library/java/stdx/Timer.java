package stdx;

public class Timer{
	private long tm = System.nanoTime() / 1000;

	public void reset(){
		tm = System.nanoTime() / 1000;
	}
	public long getTimeGap(){
		return System.nanoTime() / 1000 - tm;
	}

	public void print(){
        print(null);
	}
	public void print(String tag){
	    if (tag == null || tag.isEmpty()) tag = "time cost";
		System.out.print(tag + " : " + getTimeGap() + "us");
	}
}