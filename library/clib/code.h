#ifndef XG_HEADER_ENCODE
#define XG_HEADER_ENCODE
//////////////////////////////////////////////////////////
#include "utils.h"

#ifdef __cplusplus
extern "C" {
#endif
	u_char HexToChar(int a, int b);

	char* CharToHex(u_char ch, char* hex);

	int BASE64DecodeLength(const char* src);

	long long HexStringToInteger(const char* src);

	int BASE64Decode(const char* src, void* dest);
	
	u_int32 MD5GetUINT32(const void* src, int len);

	u_int64 MD5GetUINT64(const void* src, int len);

	u_int32 CRC32Encode(const void* src, u_int32 len);
	
	char* IntegerToHexString(long long val, char* dest);

	char* URLEncode(const char* src, int len, char* dest);

	char* URLDecode(const char* src, int len, char* dest);

	void MD5Encode(const void* src, int len, void* dest);

	char* HEXEncode(const void* src, int len, void* dest);

	u_char* HEXDecode(const char* src, int len, void* dest);

	stCharBuffer MD5GetEncodeString(const void* src, int len, BOOL upper);

	char* BASE64Encode(const void* src, int len, char* desc, BOOL addcrlf);
#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////////////////
#endif